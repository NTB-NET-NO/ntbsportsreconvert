﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.ServiceModel;
using System.Xml;

using log4net;

namespace NTB.SportsReConvert.Components
{
    public partial class MainServiceComponent : Component
    {

        static ILog logger = null;

        static MainServiceComponent()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!log4net.LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();

            logger = LogManager.GetLogger(typeof(MainServiceComponent));

            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "MainServiceComponent";
            }
        }
        public MainServiceComponent()
        {
            InitializeComponent();
        }

        public MainServiceComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        #region Instance variables and control data

        /// <summary>
        /// Name of the file that contains the job configuration set
        /// </summary>
        /// <remarks>The name of the job config XML file is stored here.</remarks>
        protected String configSet = String.Empty;

        /// <summary>
        /// Enable watching the config set for changes and reconfigure at runtime
        /// </summary>
        /// <remarks>config setting that defines if we are wathcing the config set for changes/auto reconfigure or not.</remarks>
        protected Boolean watchConfigSet = false;


        /// <summary>
        /// List of currently configured jobs
        /// </summary>
        /// <remarks>Internal Dictionary to keep track of running worker jobs.</remarks>
        protected Dictionary<String, ISportsConvertInterfaces> jobInstances = new Dictionary<String, ISportsConvertInterfaces>();


        /// <summary>
        /// Notification handler service host
        /// </summary>
        /// <remarks>
        /// This si the actuall service host that controls the notification object <see cref="ewsNotify"/> 
        /// </remarks>
        protected ServiceHost serviceHost = null;



        #endregion

        public void Configure()
        {
            try
            {
                log4net.ThreadContext.Stacks["NDC"].Push("CONFIG");

                
                // Checking if a directory exists or not
                DirectoryInfo di = new DirectoryInfo(
                    ConfigurationManager.AppSettings["OutPath"]);

                if (!di.Exists)
                {
                    di.Create();
                }


                // Read params 
                configSet = ConfigurationManager.AppSettings["ConfigurationSet"];
                watchConfigSet = Convert.ToBoolean(ConfigurationManager.AppSettings["WatchConfiguration"]);
                

                // Logging
                logger.InfoFormat("{0} : {1}", "ConfigurationSet", configSet);
                logger.InfoFormat("{0} : {1}", "WatchcConfiguration", watchConfigSet);

                // Load configuration set
                XmlDocument config = new XmlDocument();
                config.Load(configSet);

                // Setting up the watcher
                configFileWatcher.Path = System.IO.Path.GetDirectoryName(configSet);
                configFileWatcher.Filter = System.IO.Path.GetFileName(configSet);

                // Create NFF Components 
                XmlNodeList nodes = config.SelectNodes("/ComponentConfiguration/NFFComponents/NFFComponent");
                logger.InfoFormat("NFFComponent job instances found: {0}", nodes.Count);

                // Clearing all jobInstances before we populate them again. 
                jobInstances.Clear();
                foreach (XmlNode nd in nodes)
                {
                    ISportsConvertInterfaces NFFComponent = new FISComponent();
                    NFFComponent.Configure(nd);

                    logger.Debug("Adding " + NFFComponent.InstanceName);
                    jobInstances.Add(NFFComponent.InstanceName, NFFComponent);
                }

                // ... then create NIF components
                nodes = config.SelectNodes("/ComponentConfiguration/FISComponents/FISComponent");
                logger.InfoFormat("NIFComponent job instances found: {0}", nodes.Count);

                foreach (XmlNode nd in nodes)
                {
                    ISportsConvertInterfaces FISWSComponent = new FISComponent();
                    FISWSComponent.Configure(nd);

                    logger.Debug("Adding " + FISWSComponent.InstanceName);
                    jobInstances.Add(FISWSComponent.InstanceName, FISWSComponent);
                }
            }
            catch (Exception exception)
            {
                logger.Error(exception);
            }
            finally
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
            }
        }


        /// <summary>
        /// Starts the main service instance.
        /// </summary>
        /// <remarks>
        /// The function will set up polling and events to Start all processing jobs.
        /// </remarks>
        public void Start()
        {
            log4net.ThreadContext.Stacks["NDC"].Push("START");

            try
            {
                // Start instances
                logger.Debug("Number of jobs to start: " + jobInstances.Count.ToString());

                // Looping jobs
                foreach (KeyValuePair<String, ISportsConvertInterfaces> kvp in jobInstances)
                {
                    if (kvp.Value.Enabled)
                    {
                        logger.Info("Starting " + kvp.Value.InstanceName);
                        kvp.Value.Start();
                    }
                }

                // Starting maintenance
                logger.Info("Starting maintenance timer");
                maintenanceTimer.Start();

                // Watch the config file
                configFileWatcher.EnableRaisingEvents = watchConfigSet;
            }
            catch (Exception ex)
            {
                logger.Fatal("An error has occured. Could not Start service", ex);
            }

        }

        public void Pause()
        {
            maintenanceTimer.Stop();
            configFileWatcher.EnableRaisingEvents = false;

            // Stop instances
            foreach (KeyValuePair<String, ISportsConvertInterfaces> kvp in jobInstances)
            {
                if (kvp.Value.Enabled)
                {
                    kvp.Value.Stop();
                }
            }
        }

        public void Stop()
        {
            Pause();

            // Kill notification service handler
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }
        }

        

        private void maintenanceTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            logger.Debug("In maintenanceTimer_Elapsed");
            /*
             * Not sure what this shall do yet, 
             * but I would believe that getting some data would be an idea...
             */

            /**
             * We might have to add a property which says something about the state of the jobs.
             * Are they working, alive, kicking?
             * If we get an exception, what shall we do? Kill all? probably... 
             */
            // Stop instances
            //foreach (KeyValuePair<String, IBaseSportsDataInterfaces> kvp in jobInstances)
            //{
            //    // We need to find a way to get the stop-signal
            //    if (kvp.Value.Enabled)
            //    {
            //        kvp.Value.Stop();
            //    }
            //}

            
        }

        /// <summary>
        /// Reconfigures everything from an updated config set
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">File change params</param>
        /// <remarks>
        /// <para>When the configuration is updated on disk, this function triggers a complete job reload.</para>
        /// <para>Note that app.config is also reloaded, but refreshed config settings only apply to <c>AppSettings></c> here, not the other sections of the config file.</para>
        /// </remarks>
        private void configFileWatcher_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            log4net.ThreadContext.Properties["JOBNAME"] = "MainServiceWorker";

            // Outer-try-final to prevent dupe events
            try
            {
                // Stopping file watcher
                configFileWatcher.EnableRaisingEvents = false;
                logger.Info("MainServiceComponent::configFileWatcher_Changed() hit. Will reconfigure.");


                log4net.ThreadContext.Stacks["NDC"].Push("RECONFIGURE");
                //Pause everything
                Pause();

                //Give it a little break
                System.Threading.Thread.Sleep(5000);

                // Reconfigure
                ConfigurationManager.RefreshSection("applicationSettings");
                ConfigurationManager.RefreshSection("appSection");
                Configure();

                Start();


            }
            catch (Exception ex)
            {
                logger.Fatal("NTBSportsData reconfiguration failed - TERMINATING!", ex);
                throw;
            }

            finally
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
                configFileWatcher.EnableRaisingEvents = watchConfigSet;
            }

        }

    }
}

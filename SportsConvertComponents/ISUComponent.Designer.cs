﻿namespace NTB.SportsReConvert.Components
{
    partial class ISUComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pollTimer = new System.Timers.Timer();
            this.filesWatcher = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesWatcher)).BeginInit();
            // 
            // pollTimer
            // 
            this.pollTimer.Interval = 60000D;
            // 
            // filesWatcher
            // 
            this.filesWatcher.NotifyFilter = System.IO.NotifyFilters.FileName;
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesWatcher)).EndInit();

        }

        #endregion

        private System.Timers.Timer pollTimer;
        private System.IO.FileSystemWatcher filesWatcher;
    }
}

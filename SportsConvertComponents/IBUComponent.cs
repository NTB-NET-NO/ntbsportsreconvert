﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace NTB.SportsReConvert.Components
{
    public partial class IBUComponent : Component, ISportsConvertInterfaces
    {
        public IBUComponent()
        {
            InitializeComponent();
        }

        public IBUComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void Configure(System.Xml.XmlNode configNode)
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            throw new NotImplementedException();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        public bool Enabled
        {
            get { throw new NotImplementedException(); }
        }

        public string InstanceName
        {
            get { throw new NotImplementedException(); }
        }

        public OperationMode OperationMode
        {
            get { throw new NotImplementedException(); }
        }

        public PollStyle PollStyle
        {
            get { throw new NotImplementedException(); }
        }
    }
}

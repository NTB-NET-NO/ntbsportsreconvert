﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;

// Get library support
using NTB.SportsReConvert.Utilities;

// Get logging support
using log4net; 

namespace NTB.SportsReConvert.Components
{
    public partial class FISComponent : Component, ISportsConvertInterfaces
    {
        /// <summary>
        /// Static logger
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(FISComponent));

        /// <summary>
        /// Initializes the <see cref="GathererComponent"/> class.
        /// </summary>
        static FISComponent()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!log4net.LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();
        }

        public FISComponent()
        {
            InitializeComponent();
        }

        public FISComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        #region Common class variables
        /// <summary>
        /// Busy status event
        /// </summary>
        private System.Threading.AutoResetEvent _busyEvent = new System.Threading.AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private System.Threading.AutoResetEvent _stopEvent = new System.Threading.AutoResetEvent(false);

        /// <summary>
        /// The configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected String name;

        /// <summary>
        /// The enabled status for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="Enabled"/></remarks>
        protected Boolean enabled;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly configured
        /// </summary>
        /// <remarks>Holds the configured status of the instance. <c>True</c> means successfully configured</remarks>
        protected Boolean configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected Boolean errorRetry = false;


        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected String emailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected String emailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected String emailBody;

        #endregion


        #region Polling settings

        /// <summary>
        /// The polling style for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="PollStyle"/></remarks>
        protected PollStyle pollStyle;

        /// <summary>
        /// The configured schedule for this instance
        /// </summary>
        /// <remarks>This is set to a time of day and is used by the <c>Scheduled</c> pollstyle. Work is triggered at the given time of day.</remarks>
        protected String schedule;

        /// <summary>
        /// The configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int interval = 60;

        #endregion

        #region File folders

        /// <summary>
        /// Input file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="fileFilter"/> and <see cref="includeSubdirs"/>
        /// </remarks>
        protected String fileInputFolder;

        /// <summary>
        /// Error file folder
        /// </summary>
        /// <remarks>
        ///   <para>File folder where failing files are stored.</para>
        ///   <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        protected String fileErrorFolder;

        /// <summary>
        /// File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///   <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///   <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        protected String fileDoneFolder;


        #endregion

        #region Optional configuration

        /// <summary>
        /// Input file filter
        /// </summary>
        /// <remarks>
        ///   <para>Defines what file types are being read from <see cref="fileInputFolder"/>.</para>
        ///   <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        protected String fileFilter = "*.xml";

        /// <summary>
        /// Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///   <para>Defines if the instance should look in subdirectories within <see cref="fileInputFolder"/>.</para>
        ///   <para>Default value: <c>False</c></para>
        /// </remarks>
        protected Boolean includeSubdirs = false;

        /// <summary>
        /// Buffer size for the file watcher
        /// </summary>
        /// <remarks>
        ///   <para>Defines the internal buffer size the FileSystemWatcher when using this polling.
        /// Should not be set to high. The instance will fall back to contious polling on buffer overflows.</para>
        ///   <para>Default value: <c>4096</c></para>
        /// </remarks>
        protected int bufferSize = 4096;

        /// <summary>
        /// Input file format
        /// </summary>
        /// <remarks>
        ///   <para>Defins what input format to expect in the incoming files. Currently, only <c>NITF</c> is supported.</para>
        ///   <para>Default value: <c>InputFormat.NITF</c></para>
        /// </remarks>
        protected InputFormat fileFormat = InputFormat.HTML;

        /// <summary>
        /// Target public folder
        /// </summary>
        /// <remarks>
        /// Defines which public folder to put imported items in. This is an optional parameter, if empty or not set the correct value is extracted from the imported file.
        /// Functionality may be altered by setting <see cref="targetIsRoot"/>
        /// </remarks>
        protected String targetFolder = String.Empty;

        /// <summary>
        /// Set to true to use the target public folder as a root.
        /// </summary>
        /// <remarks>
        ///   <para>If this is set to <c>True</c> the folder value extracted from the imported file is appended to <see cref="targetFolder"/></para>
        ///   <para>Default value: <c>False</c></para>
        /// </remarks>
        protected Boolean targetIsRoot = false;


        /// <summary>
        /// Gets the enabled status of the instance.
        /// </summary>
        /// <value>The enabled status.</value>
        /// <remarks><c>True</c> if the job instance is enabled.</remarks>
        public Boolean Enabled
        {
            get { return enabled; }
        }

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The configured instance job name.</remarks>
        public String InstanceName
        {
            get { return name; }
        }

        /// <summary>
        /// Gets the operation mode. 
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Gatherer</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get { return OperationMode.Gatherer; }
        }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get { return pollStyle; }
        }


        #endregion 
        public void Configure(System.Xml.XmlNode configNode)
        {
            //Basic configuration sanity check
            if (configNode == null || configNode.Name != "GathererComponent" || configNode.Attributes.GetNamedItem("Name") == null)
            {
                throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
            }

            #region Basic config
            try
            {
                name = configNode.Attributes["Name"].Value;
                enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                log4net.ThreadContext.Stacks["NDC"].Push(InstanceName);

            }
            catch (Exception ex)
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing basic configuration. 'Name' and 'Enabled' are required settings", ex);
            }

            //Basic operation
            try
            {
                pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
            }
            catch (Exception ex)
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
            }

            //Email notifications
            if (configNode.Attributes.GetNamedItem("EmailNotification") != null)
                emailNotification = configNode.Attributes["EmailNotification"].Value;
            #endregion


            //Find the file folder to work with
            fileInputFolder = configNode.SelectSingleNode("XMLInputFolder").InnerText;
            logger.InfoFormat("Gatherer job - Polling: {0} / File input folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), pollStyle), fileInputFolder, enabled);

            if (enabled)
            {
                //Output email config
                logger.DebugFormat("EmailNotification: {0}", String.IsNullOrEmpty(emailNotification) ? "<none>" : emailNotification);

                #region File folders to access
                try
                {
                    //Create the file folder to work with
                    if (!Directory.Exists(fileInputFolder)) Directory.CreateDirectory(fileInputFolder);

                    //Find optional folders
                    XmlNode nd = configNode.SelectSingleNode("XMLDoneFolder");
                    if (nd != null)
                    {
                        fileDoneFolder = nd.InnerText;
                        logger.InfoFormat("DoneFolder: {0}", fileDoneFolder);
                    }

                    nd = configNode.SelectSingleNode("XMLErrorFolder");
                    if (nd != null)
                    {
                        fileErrorFolder = nd.InnerText;
                        logger.InfoFormat("ErrorFolder: {0}", fileErrorFolder);
                    }
                }
                catch (Exception ex)
                {
                    log4net.ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid, unknown or missing file folder: " + fileInputFolder, ex);
                }
                #endregion

                #region Misc. configuration
                try
                {
                    //Check for config overrides
                    if (configNode.Attributes.GetNamedItem("FileFilter") != null)
                        fileFilter = configNode.Attributes["FileFilter"].Value;
                    logger.DebugFormat("File filter: {0}", fileFilter);

                    if (configNode.Attributes.GetNamedItem("IncludeSubdirs") != null)
                        includeSubdirs = Convert.ToBoolean(configNode.Attributes["IncludeSubdirs"].Value);
                    logger.DebugFormat("Include subdirectories: {0}", includeSubdirs);

                    if (configNode.Attributes.GetNamedItem("FileFormat") != null)
                        fileFormat = (InputFormat)Enum.Parse(typeof(InputFormat), configNode.Attributes["FileFormat"].Value, true);
                    logger.DebugFormat("Input file format: {0}", Enum.GetName(typeof(InputFormat), fileFormat));

                    
                    if (configNode.Attributes.GetNamedItem("TargetIsRoot") != null)
                        targetIsRoot = Convert.ToBoolean(configNode.Attributes["TargetIsRoot"].Value);
                    logger.DebugFormat("Target is root: {0}", targetIsRoot);

                    //Find public folder settings
                    XmlNode nd = configNode.SelectSingleNode("TargetFolder");
                    if (nd != null)
                    {
                        targetFolder = nd.InnerText;
                        logger.InfoFormat("TargetFolder: {0}", targetFolder);
                    }

                }
                catch (Exception ex)
                {
                    log4net.ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid configuration setting values in XML configuration node", ex);
                }
                #endregion

                #region Set up polling
                try
                {
                    //Switch on pollstyle
                    switch (pollStyle)
                    {
                        case PollStyle.Continous:
                            interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
                            pollTimer.Interval = 5000; // short startup - interval * 1000;
                            logger.DebugFormat("Poll interval: {0} seconds", interval);
                            break;

                        case PollStyle.FileSystemWatch:
                            //Check for config overrides
                            if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                                bufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                            logger.DebugFormat("FileSystemWatcher buffer size: {0}", bufferSize);

                            //Set values
                            filesWatcher.Path = fileInputFolder;
                            filesWatcher.Filter = fileFilter;
                            filesWatcher.IncludeSubdirectories = includeSubdirs;
                            filesWatcher.InternalBufferSize = bufferSize;

                            //Do not start the event watcher here, wait until after the startup cleaning job
                            pollTimer.Interval = 5000; // short startup;
                            break;
                        default:
                            //Unsupported pollstyle for this object
                            throw new NotSupportedException("Invalid polling style for this job type");
                    }
                }
                catch (Exception ex)
                {
                    log4net.ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
                }
                #endregion
            }
            //Finish configuration
            log4net.ThreadContext.Stacks["NDC"].Pop();
            configured = true;

        }

        public void Start()
        {
            //Check status
            if (!configured)
                throw new SportsConvertExceptions("FISComponent is not properly configured. FISComponent::Start() aborted");

            if (!Enabled)
                throw new SportsConvertExceptions("FISComponent is not enabled. FISComponent::Start() aborted");

            //Set signals
            _stopEvent.Reset();
            _busyEvent.Set();
            pollTimer.Start();
        }

        public void Stop()
        {
            //Check status
            if (!configured)
                throw new SportsConvertExceptions("FISComponent is not properly configured. FISComponent::Stop() aborted");

            if (!Enabled)
                throw new SportsConvertExceptions("FISComponent is not enabled. FISComponent::Stop() aborted");

            //Check status - Handle busy polltimer loops, error loops and startup loop for FSW
            if (!pollTimer.Enabled && (errorRetry || pollStyle != Components.PollStyle.FileSystemWatch || pollTimer.Interval == 5000))
                logger.InfoFormat("Waiting for instance to complete current work. Job: {0}", InstanceName);

            //Signal stop
            _stopEvent.Set();

            //Stop events
            filesWatcher.EnableRaisingEvents = false;

            if (!_busyEvent.WaitOne(30000))
                logger.WarnFormat("Instance did not complete properly. Data may be inconsistent. Job: {0}", InstanceName);

            //Kill the polling
            pollTimer.Stop();

        }

        private void pollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            log4net.ThreadContext.Properties["JOBNAME"] = InstanceName;
            logger.DebugFormat("FISComponent::pollTimer_Elapsed() hit");

            //Reset flags and pollers
            _busyEvent.WaitOne();
            pollTimer.Stop();

            try
            {
                //Process any wating files
                List<String> files = new List<string>(Directory.GetFiles(fileInputFolder,
                    fileFilter, (includeSubdirs ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)));

                //Some logging
                logger.DebugFormat("FISComponent::pollTimer_Elapsed() Gatherer items scheduled: {0}", files.Count);

                //Enable the event listening
                if (pollStyle == PollStyle.FileSystemWatch && !errorRetry)
                    filesWatcher.EnableRaisingEvents = true;

                //Process the files
                // ProcessFileset(files);

                //Reset timer interval
                //if (pollStyle == PollStyle.Scheduled)
                //    pollTimer.Interval = Utilities.GetScheduleInterval(schedule).TotalMilliseconds;

            }
            catch (Exception ex)
            {
                logger.Error("FISComponent::pollTimer_Elapsed() error: " + ex.Message, ex);
            }

            //Check error state
            if (errorRetry)
            {
                filesWatcher.EnableRaisingEvents = false;
            }

            //Restart
            pollTimer.Interval = interval * 1000;
            if (pollStyle != PollStyle.FileSystemWatch || (pollStyle == PollStyle.FileSystemWatch && filesWatcher.EnableRaisingEvents == false))
                pollTimer.Start();

            _busyEvent.Set();
        }

        private void filesWatcher_Created(object sender, FileSystemEventArgs e)
        {
            log4net.ThreadContext.Properties["JOBNAME"] = InstanceName;
            logger.DebugFormat("FISComponent::filesWatcher_Created() hit with file data: {0}", e.FullPath);

            //Wait for the busy state
            _busyEvent.WaitOne();

            try
            {
                // ProcessSingleFile(e.FullPath);
                // MoveToArchiveFolder(e.FullPath);
            }
            catch (SportsConvertExceptions ex)
            {
                logger.Error("FISComponent::filesWatcher_Created() EWS error encountered, will retry. Message: " + ex.Message, ex);
                errorRetry = true;
                pollTimer.Start();
                filesWatcher.EnableRaisingEvents = false;
            }
            catch (Exception ex)
            {
                logger.Error("FISComponent::filesWatcher_Created() error: " + ex.Message, ex);
                // MoveToErrorFolder(e.FullPath);

                try
                {
                    if (!String.IsNullOrEmpty(emailNotification))
                    {
                        emailBody += "Fil IKKE behandlet, konvertering feilet. " + e.FullPath + ".\nFeilmelding:" + ex.Message + "\n\n" + ex.ToString();
                        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(ConfigurationManager.AppSettings["SMTPServer"]);
                        smtp.Send(ConfigurationManager.AppSettings["SMTPFromAddress"], emailNotification, emailSubject, emailBody);
                        logger.DebugFormat("Email notification sent. To: {0} Subject: {1}", emailNotification, emailSubject);
                    }
                }
                catch (Exception mex)
                {
                    logger.WarnFormat("Failed to send Email notification. To: {0} Subject: {1} Error: {2}", emailNotification, emailSubject, mex.Message);
                }

            }

            //Reset event
            _busyEvent.Set();

        }

        void filesWatcher_Error(object sender, System.IO.ErrorEventArgs e)
        {
            //Log error
            log4net.ThreadContext.Properties["JOBNAME"] = InstanceName;
            logger.ErrorFormat("FISComponent::filesWatcher_Error() error encountered: {0}", e.GetException().Message);

            //Kill file event watcher and start retry loop
            filesWatcher.EnableRaisingEvents = false;
            errorRetry = true;
            pollTimer.Start();
        }


    }
}

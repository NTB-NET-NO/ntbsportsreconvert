﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NTB.SportsReConvert.Components
{
    /// <summary>
    /// Used to specify the different operating modes. Primarily used for the common EWS poller component.
    /// </summary>
    public enum OperationMode
    {
        /// <summary>
        /// Gatherer mode collects Data web services, saves the data in an XML-file 
        /// </summary>
        Gatherer,

        /// <summary>
        /// Converter mode gets a file and converts a file and saves the information in a structured XML-file
        /// </summary>
        Converter
    }

    /// <summary>
    /// Used to tell if the service component is running or has signaled that everything has to stop
    /// </summary>
    public enum ServiceMode
    {
        /// <summary>
        /// Running mode is when everything is OK
        /// </summary>
        Running,

        /// <summary>
        /// If the program is signaling halting, the service shall stop. 
        /// </summary>
        Halting

    }


    /// <summary>
    /// Specifies the different polling styles available to the EWS poller component
    /// </summary>
    public enum PollStyle
    {
        /// <summary>
        /// Indicates continous polling og an item at a timed interval.
        /// This is the legacy pollingstyle and may be used for all job types
        /// </summary>
        Continous,
        /// <summary>
        /// Polling is limited to a time schedule, i.e. once a day/week
        /// </summary>
        Scheduled,
        /// <summary>
        /// Contious polling using an EWS folder subscription set.
        /// Allows for easier Exchange folder item checking. Only valid for EWS folder jobs.
        /// </summary>
        PullSubscribe,
        /// <summary>
        /// Notifications from EWS are actively sent to the client using a web service. Only valid for EWS folder jobs.
        /// </summary>
        PushSubscribe,
        /// <summary>
        /// Use File System Notifications with local disk folders instead of polling. Only valid for Gatherer jobs.
        /// </summary>
        FileSystemWatch,
        /// <summary>
        /// Polling happens at scheduled times of the day or once a week.
        /// </summary>
        CalendarPoll
    }
    /// <summary>
    /// Specifies the different days and which day is the first one. This so that we can support 
    /// scheduling on both day AND time
    /// </summary>


    public enum CalendarPollStyle
    {
        /// <summary>
        /// A daily CalendarPollStyle is a pullstyle that happens every day
        /// </summary>
        Daily,
        /// <summary>
        /// A daily CalendarPollStyle is a pullstyle that happens every week
        /// </summary>
        Weekly,
        /// <summary>
        /// A daily CalendarPollStyle is a pullstyle that happens every month
        /// </summary>
        Monthly,
        /// <summary>
        /// A daily CalendarPollStyle is a pullstyle that happens every year
        /// </summary>
        Yearly
    }

    /// <summary>
    /// InputFormat defines the available <c>Gatherer</c> file input formats.
    /// </summary>
    /// <remarks>
    /// <c>HTML</c> is currently the only supported format
    /// </remarks>
    public enum InputFormat
    {
        /// <summary>
        /// Read and convert XML
        /// </summary>
        XML,

        /// <summary>
        /// Read and convert messages in plain text
        /// </summary>
        TEXT,

        /// <summary>
        /// Read and convert messages in HTML
        /// </summary>
        HTML,

        /// <summary>
        /// Read and convert messages in Icalendar format
        /// </summary>
        ICMicm
    }

    public enum SingleSports
    {
        Alpint,
        Langrenn,
        Hopp,
        Kombinert,
        Snøbrett,
        Telemark,
        Tennis,
        Friidrett,
        Skiskyting
    }



}
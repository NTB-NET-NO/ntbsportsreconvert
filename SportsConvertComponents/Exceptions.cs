﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NTB.SportsReConvert.Components
{
    class SportsConvertExceptions : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrontendException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public SportsConvertExceptions(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrontendException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public SportsConvertExceptions(string message, Exception exception)
            : base(message, exception)
        { }
        
    }
}

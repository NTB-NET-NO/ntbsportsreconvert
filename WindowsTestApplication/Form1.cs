﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;
using System.IO;

namespace NTB.SportsReConvert.WindowsTestApplication
{
    public partial class frmReConverter : Form
    {
        protected SingleSports singleSports;
        protected DocumentType documentType;


        public frmReConverter()
        {
            InitializeComponent();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtReConvert.Text = Clipboard.GetText();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReConverter.ActiveForm.Close();
        }

        // Button Reconvert
        private void button1_Click(object sender, EventArgs e)
        {

            ReConverter reconverter = new ReConverter();

            reconverter.CreateOneLiners(txtReConvert.Text);
            

            // First we start by setting results on one line
            string input = txtReConvert.Text;
            
            string testInput = input;
            testInput = testInput.Replace(" og ", "# ");
            testInput = testInput.Replace(". ", "# ");
            testInput = testInput.Replace('\n', ' ').Replace('\r', ' ').Replace("  ", " "); // .Replace('\n', ' ');


            Regex testRegex = new Regex(" ([0-9]{0,3})\\)");

            testInput = testRegex.Replace(testInput, @"#$1) ");


            string[] stringLines = testInput.Split(new Char[] { '#' });

            // Looping lines
            string FinalResult = "";
            string OldRank = "";

            foreach (string stringLine in stringLines)
            {
                string Line = stringLine;
                string Result = "";

                // Cleanup area
                if (Line.IndexOf(", ") < 0)
                {
                    // let's find the place where we have letter + space + number
                    Match position = Regex.Match(Line, @"\w\s+[0-9]");
                    Line = Line.Substring(0, position.Index + 1) + ", Norge " + Line.Substring(position.Index + 1);

                }



                // Generate Rank
                // checking if the first character is a number
                int index = Line.IndexOf(')', 0);
                string firstChar = "";
                if (index > 4)
                {
                    firstChar = Line.Substring(0, 1);
                }
                else
                {
                    firstChar = Line.Substring(0, Line.IndexOf(')', 0));
                    OldRank = firstChar;
                }

                if (firstChar == " ")
                {
                    Line = OldRank + ") " + stringLine;
                }



                Result += GenerateRank(OldRank);

                // Generate name
                string Pattern = @"((?<firstname>\w+)\s+(?<lastname>\w+)|(?<firstname>\w+\s+\w+)\s+(?<lastname>\w+)),\s+";
                Regex regexPerson = new Regex(Pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
                MatchCollection matchesPerson = regexPerson.Matches(Line);

                string stringPerson = "";
                foreach (Match match in matchesPerson)
                {
                    GroupCollection groups = match.Groups;

                    string firstname = "<name.given>" + groups["firstname"] + "</name.given>\r\n";
                    string lastname = "<name.family>" + groups["lastname"] + "</name.family>\r\n";

                    stringPerson = firstname + lastname;
                }



                Result += stringPerson + "</person>";

                // Generate Country
                Pattern = @",\s+(?<country>\w+)*\s+[0-9]";

                Regex regexCountry = new Regex(Pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
                MatchCollection matchesCountry = regexCountry.Matches(Line);

                string stringCountry = "";
                foreach (Match match in matchesCountry)
                {
                    GroupCollection groups = match.Groups;
                    stringCountry = "<location>" + groups["country"].Value + "</location>";
                }
                // string stringCountry = Regex.Replace(stringRank, Pattern, CountryEvaluator);


                Result += "\r\n" + stringCountry + "\r\n";

                // Generate final time
                Pattern = @"\s+(([0-9]{1})\.([0-9]{2}),([0-9]{2})|([0-9]{2}),([0-9]{2}))(,\s+|.)";

                Regex regexFinalTime = new Regex(Pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);

                MatchCollection matchesFinalTime = regexFinalTime.Matches(Line);

                string stringFinalTime = "";

                /*
                 * <num units="time">00:24:22.1</num>
                 */
                foreach (Match match in matchesFinalTime)
                {
                    GroupCollection groups = match.Groups;
                    stringFinalTime = "<num units=\"time\">" + groups[0].Value.Trim() + "</num>\r\n";
                }

                Result += stringFinalTime;

                FinalResult += Result;


            }


            // result += @"\r\n"+stringCountry;

            txtReConvert.Text = FinalResult;
        }
        public static string GenerateCountry(Match match)
        {
            string stringCountry = match.Value;
            // "<location>" + regexCountry.Replace(Line, replacement) + "</location>";
            return stringCountry;


        }

        public static string GenerateRank(string Rank)
        {
            return "<person class=\"fis" + Rank + "\" idsrc=\"fisres\" value=\"" + Rank + "\">\r\n";
        }

        public static string GeneratePerson(Match match)
        {
            string returnval = match.Value;
            returnval = returnval.Replace(", ", "");
            returnval = returnval.Replace(",", "");

            string[] namesplit = returnval.Split(new Char[] { ' ' });
            string firstname = "<name.given>" + namesplit[0] + "</name.given>";
            string lastname = "<name.family>" + namesplit[1] + "</name.family>";

            return firstname + "\r\n" + lastname;
        }

        private void frmReConverter_FormClosing(object sender, FormClosingEventArgs e)
        {
            txtReConvert.Text = "";
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtReConvert.Text);
            // txtReConvert.Text = Clipboard.GetText();
        }

        private void filesWatcher_Created(object sender, System.IO.FileSystemEventArgs e)
        {
            // now we shall read the file and use xpath

            string filename = e.FullPath;

            XmlGetContent Content = new XmlGetContent();
            XmlSetContent NewContent = new XmlSetContent();

            Content.Filename = filename;
            NewContent.Filename = Content.getDocumentFileName();
            txtReConvert.Text = "";

            string labelText = "";
            lblSport.Text = labelText;

            try
            {
                documentType = (DocumentType)Enum.Parse(typeof(DocumentType), Content.getDocumentType(), true);

                try
                {

                    singleSports = (SingleSports)Enum.Parse(typeof(SingleSports), Content.getSport(), true);

                    txtReConvert.Text = Content.getBodyContent(filename);
                    // = Content
                }
                catch (Exception ex)
                {
                    labelText = Content.Sport;
                    // Not supportet singlesport or not a singlesport
                    // throw new ArgumentException("Invalid or missing Singlesport values in XML file", ex);
                }

            }
            catch (Exception ex)
            {
                // Don't do a thing ...
                labelText = "Unsupported Document Type! " + Content.getDocumentType();
                
            }

            lblSport.Text = labelText;
            lblSport.Visible = true;
        }
    }
}

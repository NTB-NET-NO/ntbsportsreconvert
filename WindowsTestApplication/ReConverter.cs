﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace NTB.SportsReConvert.WindowsTestApplication
{
    /// <summary>
    /// This class is used to do all the ReConverting. All methods used in here is used to generate a much better XML
    /// with more structured data
    /// </summary>
    class ReConverter
    {

        private string athleteRank = "";
        private string athleteFirstName = "";
        private string athleteLastName = "";
        private string athleteNationality = "";
        

        public String CreateOneLiners(String Input)
        {
            // First we start by setting results on one line
            string testInput = Input;
            testInput = testInput.Replace(" og ", "# ");
            testInput = testInput.Replace(". ", "# ");
            testInput = testInput.Replace('\n', ' ').Replace('\r', ' ').Replace("  ", " "); // .Replace('\n', ' ');

            return testInput;
        }

        public String Rank
        {
            set
            {
                Regex testRegex = new Regex(" ([0-9]{0,3})\\)");

                athleteRank = testRegex.Replace(value, @"#$1) ");

            }

            get
            {
                return athleteRank;
            }
        }

    }
}

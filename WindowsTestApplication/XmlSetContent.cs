﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.XPath;
using System.Xml;

namespace NTB.SportsReConvert.WindowsTestApplication
{
    class XmlSetContent
    {

        private String stringFileName = "";


        public String Filename
        {
            get
            {
                return stringFileName;
            }
            set
            {
                stringFileName = value;
            }
        }
    }
}

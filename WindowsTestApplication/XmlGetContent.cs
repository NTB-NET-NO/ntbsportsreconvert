﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.XPath;




namespace NTB.SportsReConvert.WindowsTestApplication
{
    public class XmlGetContent
    {

        public string Sport = "";

        String stringFileName = "";

        public string Filename
        {
            get
            {
                return stringFileName;
            }
            set
            {
                stringFileName = value;
            }
        }

        public string getBodyContent(string stringFileName)
        {
            string Result = "";
            XPathDocument document = new XPathDocument(stringFileName);
            XPathNavigator navigator = document.CreateNavigator();


            string xPathFindResult = @"//body/body.content/p";
            XPathNodeIterator iterator = navigator.Select(xPathFindResult);
            while (iterator.MoveNext())
            {
                Result += iterator.Current.InnerXml + "\r\n";
            }

            return Result;
        }

        public String getDocumentFileName()
        {
            XPathDocument document = new XPathDocument(stringFileName);
            XPathNavigator navigator = document.CreateNavigator();

            string xpathFindContentType = @"//meta[@name='filename']/@content";
            XPathNavigator node = navigator.SelectSingleNode(xpathFindContentType);

            if (node != null)
            {
                return node.InnerXml;
            }
            return "";

        }

        public String getDocumentType()
        {
            FileInfo fi = new FileInfo(stringFileName);

            if (fi.Exists)
            {
                XPathDocument document = new XPathDocument(stringFileName);
                XPathNavigator navigator = document.CreateNavigator();

                string xpathFindContentType = @"//tobject.property/@tobject.property.type";
                XPathNavigator node = navigator.SelectSingleNode(xpathFindContentType);

                if (node != null)
                {
                    if (node.InnerXml == "Tabeller og resultater")
                    {
                        return "Resultatmelding";
                    }
                    else
                    {
                        return "Nyhetsmelding";
                    }
                }
                return "";
            }
            return "";
        }


        public String getSport()
        {
            FileInfo fi = new FileInfo(stringFileName);

            if (fi.Exists)
            {
                XPathDocument document = new XPathDocument(stringFileName);
                XPathNavigator navigator = document.CreateNavigator();

                string xpathFindSport = @"//tobject.subject/@tobject.subject.matter";
                XPathNavigator node = navigator.SelectSingleNode(xpathFindSport);


                if (node != null)
                {
                    this.Sport = node.InnerXml;

                    return node.InnerXml;
                }
                return "";
            }

            return "";
        }


        public void Test()
        {

            string filename = @"/home/trond/Projects/Mono-develop/XmlGetContent/xml/test1.xml";
            FileInfo fi = new FileInfo(filename);
            if (fi.Exists)
            {
                XPathDocument document = new XPathDocument(filename);
                XPathNavigator navigator = document.CreateNavigator();

                string xpathFindSport = @"//tobject.subject/@tobject.subject.matter";
                XPathNavigator node = navigator.SelectSingleNode(xpathFindSport);


                if (node.InnerXml == "Alpint")
                {
                    string xPathFindResult = @"//body/body.content/p";

                    XPathNodeIterator iterator = navigator.Select(xPathFindResult);
                    while (iterator.MoveNext())
                    {

                        Console.WriteLine(iterator.Current.InnerXml);
                    }

                }
                Console.WriteLine(node.InnerXml);
            }
        }
    }

}
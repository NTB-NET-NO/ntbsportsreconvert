﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

// Adding support for log4Net logging
using log4net;
using log4net.Config;

namespace NTB.SportsReConvert.Service
{
    
    static class ServiceStart
    {
        static ILog logger = LogManager.GetLogger(typeof(MainSportsConvertService));
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            MDC.Set("JOBNAME", "NTB.SportsData.Service");
            logger.Info("Starting up the application");

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new MainSportsConvertService() 
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using NTB.SportsReConvert.Components;

using log4net;


namespace NTB.SportsReConvert.Service
{
    public partial class DebugForm : Form
    {
        /// <summary>
        /// Logger
        /// </summary>
        static ILog logger = null;

        /// <summary>
        /// The actual service object
        /// </summary>
        /// <remarks>Does the actuall work</remarks>
        protected MainServiceComponent service = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugForm"/> class.
        /// </summary>
        /// <remarks>
        /// Default constructor
        /// </remarks>
        public DebugForm()
        {
            log4net.ThreadContext.Properties["JOBNAME"] = "SportsDataService-Debug";
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(DebugForm));

            service = new MainServiceComponent();

            InitializeComponent();
        }

        private void DebugForm_Load(object sender, EventArgs e)
        {
            try
            {
                logger.Info("SportsDataService DEBUGMODE starting...");
                _startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                logger.Fatal("SportsDataService DEBUGMODE DID NOT START properly", ex);
            }
        }

        private void DebugForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                service.Stop();
                logger.Info("SportsDataService DEBUGMODE stopped");
            }
            catch (Exception ex)
            {
                logger.Fatal("SportsDataService DEBUGMODE DID NOT STOP properly", ex);
            }
        }

        private void _startupConfigTimer_Tick(object sender, EventArgs e)
        {
            //Kill the timer
            _startupConfigTimer.Stop();

            //And configure
            try
            {
                service.Configure();
                service.Start();
                logger.Info("SportsDataService DEBUGMODE started");
            }
            catch (Exception ex)
            {
                logger.Fatal("SportsDataService DEBUGMODE DID NOT START properly", ex);
            }
        }
    }
}
